# -*- coding: utf-8 -*-
"""
Created on Tue Oct 18 13:40:29 2022

@author: Administrator
"""

#////////////////////////////////////////////
#---------InsertionSortAscending-------------
#CHECKED...
def InsertionSortA(arr):
    for i in range(1, len(arr)):
        key = arr[i]
        j = i-1
        while j >= 0 and key < arr[j] :
                arr[j + 1] = arr[j]
                j -= 1
        arr[j + 1] = key
    return arr
#---------------------------------------------
#/////////////////////////////////////////////


#/////////////////////////////////////////////
#---------InsertionSortDescending------------
#CHECKED...
def InsertionSortD(Array):
    size=len(Array)
    for i in range(1,size):
        key = Array[i]
        j = i-1
        while j >= 0 and key > Array[j] :
                Array[j + 1] = Array[j]
                j -= 1
        Array[j + 1] = key
    return Array
#---------------------------------------------
#/////////////////////////////////////////////



#//////////////////////////////////////////////
#-----------MergeSortAscending-----------------     
#CHECKED...  
def mergeA(arr, l, m, r):
    num1 = m - l + 1
    num2 = r - m
    Left = [0] * (num1)
    Right = [0] * (num2)
    for i in range(0, num1):
        Left[i] = arr[l + i]
 
    for j in range(0, num2):
        Right[j] = arr[m + 1 + j]
 
   
    i = 0    
    j = 0     
    k = l     
 
    while i < num1 and j < num2:
        if Left[i] <= Right[j]:
            arr[k] = Left[i]
            i += 1
        else:
            arr[k] = Right[j]
            j += 1
        k += 1
    while i < num1:
        arr[k] = Left[i]
        i += 1
        k += 1
    while j < num2:
        arr[k] = Right[j]
        j += 1
        k += 1
def MergeSortA(arr, l, r):
    if l < r:
        m = l+(r-l)//2
        MergeSortA(arr, l, m)
        MergeSortA(arr, m+1, r)
        mergeA(arr, l, m, r)
    return arr
#---------------------------------------------
#/////////////////////////////////////////////


#//////////////////////////////////////////////
#-----------MergeSortDescending-----------------   
#CHECKED...    
def mergeD(Array, l, m, r):
    num1 = m - l + 1
    num2 = r - m
    Left = [0] * (num1)
    Right = [0] * (num2)
    for i in range(0, num1):
        Left[i] = Array[l + i]
 
    for j in range(0, num2):
        Right[j] = Array[m + 1 + j]
 
   
    i = 0    
    j = 0     
    k = l     
 
    while i < num1 and j < num2:
        if Left[i] >= Right[j]:
            Array[k] = Left[i]
            i += 1
        else:
            Array[k] = Right[j]
            j += 1
        k += 1
    while i < num1:
        Array[k] = Left[i]
        i += 1
        k += 1
    while j < num2:
        Array[k] = Right[j]
        j += 1
        k += 1
def MergeSortD(Array, l, r):
    if l < r:
        m = l+(r-l)//2
        MergeSortD(Array, l, m)
        MergeSortD(Array, m+1, r)
        mergeD(Array, l, m, r)
    return Array
#---------------------------------------------
#/////////////////////////////////////////////


#/////////////////////////////////////////////
#------------QuickSortAscending---------------   
#CHECKED...     
def partitionA(array, low, high):
    pivot = array[high]
    i = low - 1
    for j in range(low, high):
        if array[j] <= pivot:
            i = i + 1
            (array[i], array[j]) = (array[j], array[i])
    (array[i + 1], array[high]) = (array[high], array[i + 1])
    return i + 1
 
def quickSortA(array, low, high):
    if low < high:
        par = partitionA(array, low, high)
        quickSortA(array, low, par - 1)
        quickSortA(array, par + 1, high)
    return array
#----------------------------------------------
#//////////////////////////////////////////////


#/////////////////////////////////////////////
#------------QuickSortDescending---------------        
#CHECKED...
def partitionD(array, low, high):
    pivot = array[high]
    i = low - 1
    for j in range(low, high):
        if array[j] >= pivot:
            i = i + 1
            (array[i], array[j]) = (array[j], array[i])
    (array[i + 1], array[high]) = (array[high], array[i + 1])
    return i + 1
 
def QuickSortD(array, low, high):
    if low<high:
        par = partitionD(array, low, high)
        QuickSortD(array, low, par - 1)
        QuickSortD(array, par + 1, high)
    return array
#---------------------------------------------
#//////////////////////////////////////////////



#///////////////////////////////////////////////
#----------CountingSortAscending----------------
#CHECKED...
def CountingSortA(array):
    output = [0 for i in range(len(array))]
    count = [0 for i in range(256)]
    ans = ["" for _ in array]
    for i in array:
        count[ord(i)] += 1
    for i in range(256):
        count[i] += count[i-1]
    for i in range(len(array)):
        output[count[ord(array[i])]-1] = array[i]
        count[ord(array[i])] -= 1
    for i in range(len(array)):
        ans[i] = output[i]
    return ans

#--------------------------------------------------
#//////////////////////////////////////////////////



#///////////////////////////////////////////////
#----------CountingSortDescending----------------
#CHECKED...
def CountingSortD(array):
    output = [0 for i in range(len(array))]
    count = [0 for i in range(256)]
    ans = ["" for _ in array]
    for i in array:
        count[ord(i)] += 1
    for i in range(256):
        count[i] += count[i-1]
    for i in range(len(array)):
        output[count[ord(array[i])]-1] = array[i]
        count[ord(array[i])] -= 1
    for i in range(len(array)):
        ans[i] = output[i]
        Finalans=[]
        Finalans=ans[::-1]
    return Finalans

#--------------------------------------------------
#//////////////////////////////////////////////////



#///////////////////////////////////////////////////
#-----------RadixSortAscending---------------------
#CHECKED...
def countSortA(array, exp1):
 
    n = len(array)
    output = [0] * (n)
    count = [0] * (10)
    for i in range(0, n):
        index = array[i] // exp1
        count[index % 10] += 1
    for i in range(1, 10):
        count[i] += count[i - 1]
    i = n - 1
    while i >= 0:
        index = array[i] // exp1
        output[count[index % 10] - 1] = array[i]
        count[index % 10] -= 1
        i -= 1
    i = 0
    for i in range(0, len(array)):
        array[i] = output[i]
        
def RadixSortA(arr):
    maxx = max(arr)
    exp = 1
    while maxx / exp >= 1:
        countSortA(arr, exp)
        exp *= 10
    return arr
#---------------------------------------------------
#///////////////////////////////////////////////////



#///////////////////////////////////////////////////
#-----------RadixSortDescending---------------------
#CHECKED...
def countSortD(array, exp1):
 
    n = len(array)
    output = [0] * (n)
    count = [0] * (10)
    for i in range(0, n):
        index = array[i] // exp1
        count[index % 10] += 1
    for i in range(1, 10):
        count[i] += count[i - 1]
    i = n - 1
    while i >= 0:
        index = array[i] // exp1
        output[count[index % 10] - 1] = array[i]
        count[index % 10] -= 1
        i -= 1
    i = 0
    for i in range(0, len(array)):
        array[i] = output[i]
        
def RadixSortA(arr):
    maxx = max(arr)
    exp = 1
    while maxx / exp >= 1:
        countSortD(arr, exp)
        exp *= 10
    
    result=[]
    result=arr[::-1]
    return result
#---------------------------------------------------
#///////////////////////////////////////////////////




#//////////////////////////////////////////////////
#-----------------BubbleSortAscending--------------
#CHECKED...
def BubbleSortA(Array):
    size=len(Array)
    temp=0
    
    for i in range(0,size-1):
        for j in range(size-1):
            if Array[j]>Array[j+1]:
                temp=Array[j]
                Array[j]=Array[j+1]
                Array[j+1]=temp
    return Array
#--------------------------------------------------
#//////////////////////////////////////////////////


#//////////////////////////////////////////////////
#-----------------BubbleSortDescending--------------
#CHECKED...
def BubbleSortD(Array):
    size=len(Array)
    temp=0
    
    for i in range(0,size-1):
        for j in range(size-1):
            if Array[j]<Array[j+1]:
                temp=Array[j]
                Array[j]=Array[j+1]
                Array[j+1]=temp
    return Array
#--------------------------------------------------
#//////////////////////////////////////////////////



#//////////////////////////////////////////////////
#-----------------ShellSortAscending--------------
#CHECKED...
def ShellSortA(Array):
    size=len(Array)
    h=size//2
    while h>0:
        for i in range(h,size):
            temp=Array[i]
            j=i
            while j>=h and Array[j-h]>temp:
                Array[j]=Array[j-h]
                j-=h
            Array[j]=temp
        h=h//2
    return Array
#--------------------------------------------------
#//////////////////////////////////////////////////
 


#//////////////////////////////////////////////////
#-----------------ShellSortDescending--------------
#CHECKED...
def ShellSortD(Array):
    size=len(Array)
    h=size//2
    while h>0:
        for i in range(h,size):
            temp=Array[i]
            j=i
            while j>=h and Array[j-h]<temp:
                Array[j]=Array[j-h]
                j-=h
            Array[j]=temp
        h=h//2
    return Array
#--------------------------------------------------
#//////////////////////////////////////////////////


#//////////////////////////////////////////////////
#----------------SelectionSortAscending------------
#CHECKED...
def SelectionSortA(Array):
    size=len(Array)
    minimumIndex=0
    for i in range(size):
        minimumIndex=i
        for j in range(i+1,size):
            if Array[j] < Array[minimumIndex]:
                minimumIndex=j
    
        #Putting MinimumIndex At Correct Position
        (Array[i],Array[minimumIndex])=(Array[minimumIndex],Array[i])
    return Array
#//////////////////////////////////////////////////
#--------------------------------------------------


#//////////////////////////////////////////////////
#----------------SelectionSortDescending------------
#CHECKED...
def SelectionSortD(Array):
    size=len(Array)
    minimumIndex=0
    for i in range(size):
        minimumIndex=i
        for j in range(i+1,size):
            if Array[j] > Array[minimumIndex]:
                minimumIndex=j
    
        #Putting MinimumIndex At Correct Position
        (Array[i],Array[minimumIndex])=(Array[minimumIndex],Array[i])
    return Array

#//////////////////////////////////////////////////
#--------------------------------------------------


#//////////////////////////////////////////////////
#--------------HeapSortAscending------------------
#CHECKED...
def Heapify(Array,size,i):
    maximum=i
    leftChild=2*i+1
    rightChild=2*i+2
    if leftChild<size and Array[maximum]<Array[leftChild]:
        maximum=leftChild
    
    if rightChild<size and Array[maximum]<Array[rightChild]:
        maximum=rightChild
    
    if maximum!=i:
        #SWAP
       (Array[i],Array[maximum]) = (Array[maximum],  Array[i])
       Heapify(Array,size,maximum)
        
def HeapSortA(Array,size):
    
    for i in range(int(size/2)-1,-1,-1):
        Heapify(Array,size,i)
    for i in range(size-1,-1,-1):
        #SWAP
        Array[0], Array[i] = Array[i], Array[0]
        Heapify(Array,i,0)
    return Array
#--------------------------------------------------
#//////////////////////////////////////////////////


#//////////////////////////////////////////////////
#--------------HeapSortDescending------------------
#CHECKED...
def Heapify(Array,size, i):
    minimum = i 
    leftChild = 2 * i + 1 
    rightChild = 2 * i + 2 
    if leftChild < size and Array[leftChild] < Array[minimum]:
        minimum = leftChild
    if rightChild < size and Array[rightChild] < Array[minimum]:
        minimum = rightChild
    if minimum != i:
        (Array[i],
         Array[minimum]) = (Array[minimum],
                           Array[i])
        Heapify(Array, size, minimum)
def HeapSortD(Array, size):
    for i in range(int(size/ 2) - 1, -1, -1):
        Heapify(Array, size, i)
    for i in range(size-1, -1, -1):
        Array[0], Array[i] = Array[i], Array[0]
        Heapify(Array, i, 0)
    return Array
#--------------------------------------------------
#//////////////////////////////////////////////////


#/////////////////////////////////////////////////////
#----------BubbleSortAscendingString-----------------
#CHECKED...
def BubbleSortStringA(array):
    n=len(array)
    for i in range(n):
        for j in range(n-i-1):
            if array[j]>array[j+1]:
                array[j], array[j + 1] = array[j + 1], array[j]
    return array
#----------------------------------------------------
#////////////////////////////////////////////////////

#/////////////////////////////////////////////////////
#----------BubbleSortDescendingString-----------------
#CHECKED...
def BubbleSortStringD(array):
    n=len(array)
    for i in range(n):
         for j in range(n-i-1):
             if array[j]<array[j+1]:
                 array[j], array[j + 1] = array[j + 1], array[j]
    return array
 #----------------------------------------------------
 #////////////////////////////////////////////////////

#/////////////////////////////////////////////////////
#-----------------BucketSortAscending-----------------
#CHECKED...
import random
 
def BucketSortA(Array):
    large=max(Array)
    size=len(Array)
    temp=large/size
    Bucket = [[] for i in range(size)]
    
    for i in range(size):
        index = int(Array[i]/temp)
        if index != size:
            Bucket[index].append(Array[i])
        else:
            Bucket[size - 1].append(Array[i])
    for i in range(size):
        Bucket[i] = sorted(Bucket[i])
        
    output = []
    for i in range(size):
        output = output + Bucket[i]
             
    return output
#-----------------------------------------------------
#/////////////////////////////////////////////////////

#//////////////////////////////////////////////////////
#-------------BucketSortDescending---------------------
#CHECKED...
import random
 
def BucketSortA(Array):
    large=max(Array)
    size=len(Array)
    temp=large/size
    Bucket = [[] for i in range(size)]
    
    for i in range(size):
        index = int(Array[i]/temp)
        if index != size:
            Bucket[index].append(Array[i])
        else:
            Bucket[size-1 ].append(Array[i])
    for i in range(size):
        Bucket[i] = sorted(Bucket[i])
    output = []
    for i in range(size):
        output = output + Bucket[i]
    result=output[::-1]         
    return result
#----------------------------------------------------------
#//////////////////////////////////////////////////////////


#/////////////////////////////////////////////////////////
#------------------CombSortAscending(String+Int)----------
#CHCKED...
def NextGapA(gap):
    gap=(gap*10)//13
    if gap<1:
        return 1
    return gap

def CombSortA(Array):
    size = len(Array)
    gap=size
    flag=True
    while gap!=1 or flag==True:
        gap=NextGapA(gap)
        flag=False
        for i in range(0,size-gap):
            if Array[i]>Array[i+gap]:
                Array[i],Array[i+gap]=Array[i+gap],Array[i]
                flag=True
    return Array
#-----------------------------------------------------------
#///////////////////////////////////////////////////////////


#/////////////////////////////////////////////////////////
#------------------CombSortDescending(String+Int)----------
#CHECKED...
def NextGapD(gap):
    gap=(gap*10)//13
    if gap<1:
        return 1
    return gap

def CombSortD(Array):
    size = len(Array)
    gap=size
    flag=True
    while gap!=1 or flag==True:
        gap=NextGapD(gap)
        flag=False
        for i in range(0,size-gap):
            if Array[i]<Array[i+gap]:
                Array[i],Array[i+gap]=Array[i+gap],Array[i]
                flag=True
    return Array
#-----------------------------------------------------------
#///////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////
#---------------PiegonSortA---------------------------------
#CHECKED...
def PiegonSortA(Array):
    smallest=min(Array)
    largest=max(Array)
    
    size=largest-smallest +1
    holes=[0]*size
    for i in Array:
        holes[i-smallest]+=1
    i=0
    for j in range(size):
        while holes[j]>0:
            holes[j]-=1
            Array[i] = j + smallest
            i += 1
    return Array
#------------------------------------------------------------
#////////////////////////////////////////////////////////////            


#///////////////////////////////////////////////////////////
#---------------PiegonSortDescending-----------------------
#CHECKED...
def PiegonSortD(Array):
    smallest=min(Array)
    largest=max(Array)
    
    size=largest-smallest +1
    holes=[0]*size
    for i in Array:
        holes[i-smallest]+=1
        
    i=size-1
    k=0
    for j in reversed(range(size)):
        while holes[j]>0:
            holes[j]-=1
            Array[k] = j + smallest
            i -= 1
            k += 1
    return Array
#----------------------------------------------------------
#//////////////////////////////////////////////////////////

    
    
    
