#-------CountingSortAscending----------------
def CountingSortA(Array):
    maximum=max(Array)
    size = len(Array)
    Array2=[0]*size
    Array3=[0]*(maximum+1)
    for i in range(size):
        Array3[Array[i]] += 1
    for i in range(1,maximum+1):
        Array3[i] += Array3[i-1]
        
    i = size-1
    while i>=0:
        Array2[Array3[Array[i]]-1] = Array[i]
        Array3[Array[i]] -= 1
        i -= 1
        
    for i in range(0,size):
        Array[i]=Array2[i]
        
    return Array
#------------------------------------------------

#----------CountingSortDescending----------------
def CountingSortD(Array):
    maximum=max(Array)
    size = len(Array)
    Array2=[0]*size
    Array3=[0]*(maximum+1)
    for i in range(size):
        Array3[Array[i]] += 1
    for i in range(maximum,0,-1):
        Array3[i-1] += Array3[i]
        
    for j in range(0,size):
            Array2[Array3[Array[j]]-1] = Array[j]
            Array3[Array[j]] -= 1
            
        
    for i in range(0,size):
        Array[i]=Array2[i]
        
    return Array
#------------------------------------------------
