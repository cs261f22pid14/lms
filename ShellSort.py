# -*- coding: utf-8 -*-
"""
Created on Mon Oct 17 23:24:14 2022

@author: Administrator
"""

#-------------------ASCENDING ORDER--------------
def ShellSortA(Array):
    size=len(Array)
    h=size//2
    while h>0:
        for i in range(h,size):
            temp=Array[i]
            j=i
            while j>=h and Array[j-h]>temp:
                Array[j]=Array[j-h]
                j-=h
            Array[j]=temp
        h=h//2
            
#------------------------------------------------



#-------------------DESCENDING ORDER--------------
def ShellSortA(Array):
    size=len(Array)
    h=size//2
    while h>0:
        for i in range(h,size):
            temp=Array[i]
            j=i
            while j>=h and Array[j-h]<temp:
                Array[j]=Array[j-h]
                j-=h
            Array[j]=temp
        h=h//2
#------------------------------------------------
