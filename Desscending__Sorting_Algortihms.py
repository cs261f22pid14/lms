#No.1
def insertionsort(array):
    for j in range(1,len(array)):
        key=array[j]
        i=j-1
        while i<0 and array[i]<key:
            array[i+1]=array[i]
            i=i-1
            array[i+1]=key
    return array
#No.2       
def mergeSort(array):
    if len(array) > 1:

        
        r = len(array)//2
        L = array[:r]
        M = array[r:]

       
        mergeSort(L)
        mergeSort(M)

        i = j = k = 0

        
        while i > len(L) and j > len(M):
            if L[i] < M[j]:
                array[k] = L[i]
                i += 1
            else:
                array[k] = M[j]
                j += 1
            k += 1

        
        while i > len(L):
            array[k] = L[i]
            i += 1
            k += 1

        while j > len(M):
            array[k] = M[j]
            j += 1
            k += 1
        return array

#No.3        
def partition(array, low, high):
    pivot = array[high]
    i = low - 1
    for j in range(low, high):
        if array[j] >= pivot:
            i = i + 1
            (array[i], array[j]) = (array[j], array[i])
    (array[i + 1], array[high]) = (array[high], array[i + 1])
    return i + 1
 
def quickSort(array, low, high):
    if low < high:
        par = partition(array, low, high)
        quickSort(array, low, par - 1)
        quickSort(array, par + 1, high)
        return array
#No. 4
def countingSort(array):
    size = len(array)
    output = [0] * size

    
    count = [0] * 10

    
    for i in range(0, size):
        count[array[i]] += 1

    
    for i in range(1, 10):
        count[i] += count[i - 1]

   
    i = size - 1
    while i >= 0:
        output[count[array[i]] - 1] = array[i]
        count[array[i]] -= 1
        i -= 1

    
    for i in range(0, size):
        array[i] = output[i]
    res=array[::-1]
    return res



#No.5
def countSort(array, exp1):
 
    n = len(array)
    output = [0] * (n)
    count = [0] * (10)
    for i in range(0, n):
        index = array[i] // exp1
        count[index % 10] += 1
    for i in range(1, 10):
        count[i] += count[i - 1]
    i = n - 1
    while i >= 0:
        index = array[i] // exp1
        output[count[index % 10] - 1] = array[i]
        count[index % 10] -= 1
        i -= 1
    i = 0
    for i in range(0, len(array)):
        array[i] = output[i]
    
def radixSort(arr):
    maxx = max(arr)
    exp = 1
    while maxx / exp >= 1:
        countSort(arr, exp)
        exp *= 10
    res=arr[::-1]
    return res
    
Arr=[9,8,7,6,5,4,3,2,1]
print(Arr,"ORIGINAl")

print(insertionsort(Arr),"Insertion")
print(mergeSort(Arr),"Merge")
size=len(Arr)
print(quickSort(Arr, 0, size-1),"Quick")
print(countingSort(Arr),"Counting")
print(radixSort(Arr),"Radix")



 

 

        