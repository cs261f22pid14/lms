# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 22:47:26 2022

@author: Administrator
"""

#///////////////////////////////////////////////
#-----BRUTEFORCE-------------------------------
def BruteForce(text,key):
    sizeText=len(text)
    sizeKey=len(key)
    for i in range(1+(sizeText-sizeKey)):
        flag=True
        for j in range(sizeKey):
            if text[i+j]!=key[j]:
                flag=False
                break
        if flag:
            return i

#INPUT EXAMPLE
# result=BruteForce(text="hello world", key="o")
#--------------------------------------------------
#//////////////////////////////////////////////////


#/////////////////////////////////////////////////
#----------------BinarySearch----------------------
def BinarySearch(Array,key,low,high):   #low=0 and high = len(Array)-1 key is element to be found
    while high>=low:
        mid = low + (high-low)//2
        if Array[mid]==key:
            return mid
        elif Array[mid]<key:
            low=mid+1
        else:
            high=mid-1
    return -1

#---------------------------------------------------
#////////////////////////////////////////////////////


#////////////////////////////////////////////////////
#---------------LinearSearch-------------------------
def LinearSearch(Array,key): #key is the element to be found
    size=len(Array)
    for i in range(0,size):
        if(Array[i]==key):
            return i
    return -1               #write condition in main if return is -1 print(NOT FOUND) 


#-----------------------------------------------------
#/////////////////////////////////////////////////////