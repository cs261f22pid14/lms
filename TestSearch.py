def ExactLinearSearch(Array,Key):
    for i in range(len(Array)):
        if Array[i]==Key:
            return i
    return -1

def ContainsLinearSearch(Array,Key):
    for i in range(len(Array)):
        if Array[i].find(Key)==-1:
            continue
        else:
            return i
def StartsLinearSearch(Array,Key):
    for i in range(len(Array)):
        if  Array[i].startswith(Key):
            return i
    return -1

def EndsLinearSearch(Array,Key):
    for i in range(len(Array)):
        if  Array[i].endswith(Key)==True:
            return i
    return -1

#FOR TEST PURPOSE
def main():
    Array=["Abdullah","Faizan","Ammar","Eihaab","Zubair"]
    output=EndsLinearSearch(Array,"h")
    print(output)
    
if __name__=="__main__":
    main()
