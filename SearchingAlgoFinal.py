# -*- coding: utf-8 -*-
"""
Created on Tue Oct 25 09:35:15 2022

@author: Administrator
"""

def ExactSearch(Array,Key):
    for i in range(len(Array)):
        if Array[i]==Key:
            return i
    return -1

def ContainsSearch(Array,Key):
    
    if len(Key)==1:
        for i in range(len(Array)):
            charArray=[char for char in Array[i]]
            for j in range(len(charArray)):
                 if charArray[j]==Key:
                     return i
    if len(Key)!=1:
        keyArray=[char for char in Key]
        for i in range(len(Array)):
            result=[char for char in Array[i]]
            for k in range(len(result)-1):
                for j in range(len(keyArray)-1):
                    if result[k]==keyArray[j] and result[k+1]==keyArray[j+1]:
                        return i
    return -1     
 
def StartsSearch(Array,Key):
    keyArray=[char for char in Key]
    length=len(Key)
    for i in range(len(Array)):
        count=0
        result=[char for char in Array[i]]
        for j in range(length):
            if result[j]==keyArray[j]:
                count = count+1
            if count==length:
                return i
    return -1

def EndsSearch(Array,Key):
    keyArray=[char for char in Key]
    RkeyArray=keyArray[::-1]
    length=len(Key)
    for i in range(len(Array)):
        count=0
        result=[char for char in Array[i]]
        ReverseResult=result[::-1]
        for j in range(length):
            if ReverseResult[j]==RkeyArray[j]:
                count = count+1
            if count==length:
                return i
    return -1

#FOR TEST PURPOSE
def main():
    Array=["Abdullah","Faizan","Ammar","Eihaab","Zubair"]
    output=ContainsSearch(Array,"A")
    print(output)
    
if __name__=="__main__":
    main()
