#/////////////////////////////////////////////////////
#-----------------BucketSortAscending-----------------
import random
 
def BucketSortA(Array):
    large=max(Array)
    size=len(Array)
    temp=large/size
    Bucket = [[] for i in range(size)]
    
    for i in range(size):
        index = int(Array[i]/temp)
        if index != size:
            Bucket[index].append(Array[i])
        else:
            Bucket[size - 1].append(Array[i])
    for i in range(size):
        Bucket[i] = sorted(Bucket[i])
        
    output = []
    for i in range(size):
        output = output + Bucket[i]
             
    return output
#-----------------------------------------------------
#/////////////////////////////////////////////////////

#//////////////////////////////////////////////////////
#-------------BucketSortDescending---------------------
import random
 
def BucketSortA(Array):
    large=max(Array)
    size=len(Array)
    temp=large/size
    Bucket = [[] for i in range(size)]
    
    for i in range(size):
        index = int(Array[i]/temp)
        if index != size:
            Bucket[index].append(Array[i])
        else:
            Bucket[size-1 ].append(Array[i])
    for i in range(size):
        Bucket[i] = sorted(Bucket[i])
    output = []
    for i in range(size):
        output = output + Bucket[i]
    result=output[::-1]         
    return result
#----------------------------------------------------------
#//////////////////////////////////////////////////////////
