#No.1
def insertionSort(arr):
    for i in range(1, len(arr)):
        key = arr[i]
        j = i-1
        while j >= 0 and key < arr[j] :
                arr[j + 1] = arr[j]
                j -= 1
        arr[j + 1] = key
#No.2       
def merge(arr, l, m, r):
    num1 = m - l + 1
    num2 = r - m
    Left = [0] * (num1)
    Right = [0] * (num2)
    for i in range(0, num1):
        Left[i] = arr[l + i]
 
    for j in range(0, num2):
        Right[j] = arr[m + 1 + j]
 
   
    i = 0    
    j = 0     
    k = l     
 
    while i < num1 and j < num2:
        if Left[i] <= Right[j]:
            arr[k] = Left[i]
            i += 1
        else:
            arr[k] = Right[j]
            j += 1
        k += 1
    while i < num1:
        arr[k] = Left[i]
        i += 1
        k += 1
    while j < num2:
        arr[k] = Right[j]
        j += 1
        k += 1
def mergeSort(arr, l, r):
    if l < r:
        m = l+(r-l)//2
        mergeSort(arr, l, m)
        mergeSort(arr, m+1, r)
        merge(arr, l, m, r)
#No.3        
def partition(array, low, high):
    pivot = array[high]
    i = low - 1
    for j in range(low, high):
        if array[j] <= pivot:
            i = i + 1
            (array[i], array[j]) = (array[j], array[i])
    (array[i + 1], array[high]) = (array[high], array[i + 1])
    return i + 1
 
def quickSort(array, low, high):
    if low < high:
        par = partition(array, low, high)
        quickSort(array, low, par - 1)
        quickSort(array, par + 1, high)
#No. 4
def countingSort(array):
    output = [0 for i in range(len(array))]
    count = [0 for i in range(256)]
    ans = ["" for _ in array]
    for i in array:
        count[ord(i)] += 1
    for i in range(256):
        count[i] += count[i-1]
    for i in range(len(array)):
        output[count[ord(array[i])]-1] = array[i]
        count[ord(array[i])] -= 1
    for i in range(len(array)):
        ans[i] = output[i]
    return ans 
#No.5
def countSort(array, exp1):
 
    n = len(array)
    output = [0] * (n)
    count = [0] * (10)
    for i in range(0, n):
        index = array[i] // exp1
        count[index % 10] += 1
    for i in range(1, 10):
        count[i] += count[i - 1]
    i = n - 1
    while i >= 0:
        index = array[i] // exp1
        output[count[index % 10] - 1] = array[i]
        count[index % 10] -= 1
        i -= 1
    i = 0
    for i in range(0, len(array)):
        array[i] = output[i]
def radixSort(arr):
    maxx = max(arr)
    exp = 1
    while maxx / exp >= 1:
        countSort(arr, exp)
        exp *= 10

 

 

        